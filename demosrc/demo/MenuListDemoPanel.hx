package demo;
import com.haxepunk.graphics.Image;
import com.haxepunk.gui.Button;
import com.haxepunk.gui.CheckBox;
import com.haxepunk.gui.Control;
import com.haxepunk.gui.event.ControlEvent;
import com.haxepunk.gui.Label;
import com.haxepunk.gui.MenuItem;
import com.haxepunk.gui.MenuList;
import com.haxepunk.gui.Panel;
import com.haxepunk.gui.RadioButton;
import com.haxepunk.gui.ToggleButton;
import com.haxepunk.utils.Input;
import com.haxepunk.utils.Key;
import openfl.Assets;
import flash.geom.Point;
import flash.text.TextFormatAlign;
import flash.events.Event;

/**
 * ...
 * @author Lythom
 */

class MenuListDemoPanel extends DemoPanel
{

	private var ml:MenuList;
	private var visibleSubMenu:MenuList;
	private var subMenus:Array<MenuList>;

	public function new(width:Int, height:Int)
	{
		super(0, 0, width, height, true);

		subMenus = new Array<MenuList>();

		var cursor:Point = new Point(10, 10);
		var margin:Int = 10;


		cursor.x = globalInstructionLabel.localX + globalInstructionLabel.width + margin;
		var l:Label = new Label("MenuList and MenuItem", cursor.x, cursor.y, Math.round(width - cursor.x));
		l.align = TextFormatAlign.CENTER;
		l.size = 16;
		l.color = 0x000000;
		addControl(l);

		// Menu List
		cursor.y += margin + l.height;
		cursor.x = globalInstructionLabel.localX + globalInstructionLabel.width + margin + 30;
		ml = new MenuList(cursor.x, cursor.y, 0);
		ml.selectorPadding = 2;

		// menu Items
		for (i in 0...7)
		{
			var l:MenuItem = new MenuItem(
				"Item " + String.fromCharCode(Math.floor(Math.random() * 40 + Key.A-1)),
				new Image("gfx/test/"+i+".png"), Math.floor(Math.random()*50*i), 0, 0, 80, 20);
			l.quantityLabel.color = 0x9F2B4E;
			l.quantityLabel.autoWidth = false;
			l.quantityLabel.width = 24;
			ml.addControl(l);
		}
		// on submenu on click
		ml.addEventListener(MenuList.CLICKED, openSubMenuEvt);
		addControl(ml);

		cursor.y = globalInstructionLabel.y + globalInstructionLabel.height + margin;
		cursor.x = globalInstructionLabel.x;
		var instructions:String = "Specific Instructions :\n";
		instructions += "up arrow - move selection up\n";
		instructions += "down arrow -  move selection down\n";
		instructions += "enter - open submenu\n";
		l = new Label(instructions, cursor.x, cursor.y);
		addControl(l);
	}

	private function openSubMenuEvt(e:ControlEvent):Void
	{
		var m:MenuList = cast(e.control, MenuList);
		openSubMenu(m);
	}
	private function openSubMenu(m:MenuList):Void
	{
		if (visibleSubMenu != null && visibleSubMenu.visible) {
			visibleSubMenu.hide();
		}

		if (subMenus[m.selectedId] == null) {
			subMenus[m.selectedId] = new MenuList();
			subMenus[m.selectedId].selectorPadding = 2;
			subMenus[m.selectedId].addControl(new Label("Item " + m.selectedId + ".1", 0, 0, 0, 20));
			subMenus[m.selectedId].addControl(new Label("Item " + m.selectedId + ".2", 0, 0, 0, 20));
			subMenus[m.selectedId].addControl(new Label("Item " + m.selectedId + ".3", 0, 0, 0, 20));
			subMenus[m.selectedId].addEventListener(MenuList.MOUSE_OUT, closeMenu);
			subMenus[m.selectedId].addEventListener(MenuList.CLICKED, closeMenu);
			subMenus[m.selectedId].layer = m.layer - m.depth;
			visibleSubMenu = subMenus[m.selectedId];
			subMenus[m.selectedId].hide();
			subMenus[m.selectedId].addEventListener(MenuList.ADDED_TO_WORLD, showSubmenu);
			subMenus[m.selectedId].stickToCamera = true;
			m.scene.add(subMenus[m.selectedId]);
		} else {
			visibleSubMenu = subMenus[m.selectedId];
			showSubmenu();
		}

	}

	private function showSubmenu(?e:Event):Void
	{
		if (visibleSubMenu.hasEventListener(MenuList.ADDED_TO_WORLD)) {
			visibleSubMenu.removeEventListener(MenuList.ADDED_TO_WORLD, showSubmenu);
		}
		visibleSubMenu.layer = ml.layer - ml.depth;
		visibleSubMenu.localX = Math.round(ml.localX + ml.width);
		visibleSubMenu.localY = Math.round(ml.localY + ml.selector.y - ml.selectorPadding);
		visibleSubMenu.show();

	}

	private function closeMenu(e:ControlEvent=null):Void
	{
		if (visibleSubMenu != null && visibleSubMenu.visible) {
			visibleSubMenu.hide();
		}
		visibleSubMenu = null;
	}

	override public function addControl(child:Control, ?position:Int):Void
	{
		super.addControl(child, position);
	}

	override public function update()
	{
		if (!enabled) {
			return;
		}

		if (Input.pressed(Key.UP)) {
			if (visibleSubMenu != null) {
				visibleSubMenu.selectedId--;
			} else {
				ml.selectedId--;
			}
		}
		if (Input.pressed(Key.DOWN)) {
			if (visibleSubMenu != null) {
				visibleSubMenu.selectedId++;
			} else {
				ml.selectedId++;
			}
		}
		if (Input.pressed(Key.ENTER)) {
			if (visibleSubMenu != null) {
				closeMenu();
			} else {
				openSubMenu(ml);
			}
		}

		super.update();
	}

}